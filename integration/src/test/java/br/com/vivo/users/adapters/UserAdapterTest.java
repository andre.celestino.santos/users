package br.com.vivo.users.adapters;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.vivo.users.entities.TypeEntity;
import br.com.vivo.users.entities.UserEntity;
import br.com.vivo.users.models.User;
import br.com.vivo.users.models.UserType;
import br.com.vivo.users.parses.UserEntityToUserParse;
import br.com.vivo.users.parses.UserToUserEntityParse;
import br.com.vivo.users.repositories.TypeRepository;
import br.com.vivo.users.repositories.UserRepository;

@ExtendWith(SpringExtension.class)
public class UserAdapterTest {
	
	@InjectMocks
	private UserAdapter userAdapter;
	
	@Mock
	private UserRepository userRepository;

	@Mock
	private TypeRepository typeRepository;

	@Mock
	private UserToUserEntityParse userToUserEntityParse;

	@Mock
	private UserEntityToUserParse userEntityToUserParse;
	
	@Test
	public void saveOk() {
		User user = new User();
		UserType type = new UserType();
		type.setId(1);
		user.setType(type);
		
		UserEntity entity = new UserEntity();
		TypeEntity typeEntity = new TypeEntity();
		typeEntity.setDescription("cientista");
		entity.setType(typeEntity);
		
		Mockito.when(typeRepository.findById(user.getType().getId())).thenReturn(Optional.of(typeEntity));
		
		Mockito.when(userRepository.save(null)).thenReturn(entity);
		
		userAdapter.save(user);
		
		ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
		
		Mockito.verify(userEntityToUserParse).to(captor.capture());
		
		Assertions.assertEquals("cientista", captor.getValue().getType().getDescription());
	}

}