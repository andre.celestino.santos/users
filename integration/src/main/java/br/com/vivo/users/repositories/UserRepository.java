package br.com.vivo.users.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.vivo.users.entities.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {}
