package br.com.vivo.users.parses;

import org.springframework.stereotype.Component;

import br.com.vivo.users.entities.TypeEntity;
import br.com.vivo.users.entities.UserEntity;
import br.com.vivo.users.models.User;
import br.com.vivo.users.models.UserType;

@Component
public class UserEntityToUserParse {

	public User to(UserEntity entity) {
		User user = new User();
		user.setCity(entity.getCity());
		user.setCountry(entity.getCountry());
		user.setId(entity.getId());
		user.setName(entity.getName());
		user.setState(entity.getState());
		user.setType(to(entity.getType()));
		return user;
	}
	
	private UserType to(TypeEntity entity) {
		UserType userType = new UserType();
		userType.setDescripton(entity.getDescription());
		userType.setId(entity.getId());
		return userType;
	}
	
}
