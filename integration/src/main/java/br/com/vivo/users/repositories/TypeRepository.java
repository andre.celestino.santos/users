package br.com.vivo.users.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.vivo.users.entities.TypeEntity;

@Repository
public interface TypeRepository extends CrudRepository<TypeEntity, Integer> {}
