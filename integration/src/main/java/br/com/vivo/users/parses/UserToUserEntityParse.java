package br.com.vivo.users.parses;

import org.springframework.stereotype.Component;

import br.com.vivo.users.entities.TypeEntity;
import br.com.vivo.users.entities.UserEntity;
import br.com.vivo.users.models.User;
import br.com.vivo.users.models.UserType;

@Component
public class UserToUserEntityParse {
	
	public UserEntity to(User user) {
		UserEntity entity = new UserEntity();
		entity.setCity(user.getCity());
		entity.setCountry(user.getCountry());
		entity.setName(user.getName());
		entity.setState(user.getState());
		entity.setType(to(user.getType()));
		return entity;
	}
	
	private TypeEntity to(UserType type) {
		TypeEntity entity = new TypeEntity();
		entity.setId(type.getId());
		return entity;
	}

}