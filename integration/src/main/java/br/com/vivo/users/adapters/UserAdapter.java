package br.com.vivo.users.adapters;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vivo.users.entities.TypeEntity;
import br.com.vivo.users.entities.UserEntity;
import br.com.vivo.users.exceptions.RegisterNotFountException;
import br.com.vivo.users.models.User;
import br.com.vivo.users.parses.UserEntityToUserParse;
import br.com.vivo.users.parses.UserToUserEntityParse;
import br.com.vivo.users.ports.UserPort;
import br.com.vivo.users.repositories.TypeRepository;
import br.com.vivo.users.repositories.UserRepository;

@Service
public class UserAdapter implements UserPort {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TypeRepository typeRepository;

	@Autowired
	private UserToUserEntityParse userToUserEntityParse;

	@Autowired
	private UserEntityToUserParse userEntityToUserParse;

	@Override
	public User save(User user) {
		try {
			Optional<TypeEntity> type = typeRepository.findById(user.getType().getId());

			if (!type.isPresent()) {
				throw new RegisterNotFountException("tipo de usuario nao encontrado para o id: " + user.getType().getId());
			}
			
			UserEntity userEntity = userToUserEntityParse.to(user);
			
			UserEntity saved = userRepository.save(userEntity);
			saved.getType().setDescription(type.get().getDescription());
			
			return userEntityToUserParse.to(saved);
		} catch (Exception e) {
			throw new RuntimeException("ocorreu um erro no acesso a base de dados: " + e.getMessage());
		}
	}
}
