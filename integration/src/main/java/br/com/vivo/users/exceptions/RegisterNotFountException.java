package br.com.vivo.users.exceptions;

public class RegisterNotFountException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public RegisterNotFountException(String message){
		super(message);
	}

}
