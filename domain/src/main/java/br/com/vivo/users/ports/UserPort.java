package br.com.vivo.users.ports;

import org.springframework.stereotype.Service;

import br.com.vivo.users.models.User;

@Service
public interface UserPort {

	public User save(User user);
	
}
