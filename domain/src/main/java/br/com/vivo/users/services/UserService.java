package br.com.vivo.users.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vivo.users.exceptions.BusinessRulesException;
import br.com.vivo.users.models.User;
import br.com.vivo.users.ports.UserPort;

/*
 * classe responsavel por aplicar alguma regra de negocio para o modelo
 */

@Service
public class UserService {
	
	private static final int LENGTH_NAME = 30;
	
	@Autowired
	private UserPort port;

	// aqui sera possivel incluir qualquer regra de negocio na gravacao de um usuario
	public User save(User user) {
		if(user.getName().length() > LENGTH_NAME) {
			throw new BusinessRulesException("não é possivel registrar um usuario com nome maior que " + LENGTH_NAME);
		}
		return port.save(user);
	}

}