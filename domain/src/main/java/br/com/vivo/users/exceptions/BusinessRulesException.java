package br.com.vivo.users.exceptions;

public class BusinessRulesException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BusinessRulesException(String message){
		super(message);
	}
	
}
