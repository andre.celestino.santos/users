package br.com.vivo.users.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {
	
	@JsonProperty("codigo")
	private long id;
	
	@JsonProperty("nome")
	private String name;
	
	@JsonProperty("cidade")
	private String city;
	
	@JsonProperty("estado")
	private String state;
	
	@JsonProperty("pais")
	private String country;
	
	@JsonProperty("tipo_usuario")
	private UserTypeDto userType;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public UserTypeDto getUserType() {
		return userType;
	}
	public void setUserType(UserTypeDto userType) {
		this.userType = userType;
	}
	
}