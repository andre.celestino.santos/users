package br.com.vivo.users.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.vivo.users.dtos.UserDto;
import br.com.vivo.users.models.User;
import br.com.vivo.users.parses.UserDtoToUserParse;
import br.com.vivo.users.parses.UserToUserDtoParse;
import br.com.vivo.users.services.UserService;

@RestController
@RequestMapping("/users")
public class UserEndpoint {
	
	@Autowired
	private UserService service;
	
	@Autowired
	private UserDtoToUserParse userDtoToUserParse;
	
	@Autowired
	private UserToUserDtoParse userToUserDtoParse;

	@PostMapping
	public ResponseEntity<?> save(@RequestBody UserDto dto) {
		// exemplo para validacao de dados na request
		if(dto == null || dto.getId() > 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "nao é possivel salvar um usuario com o codigo preenchido ou dados nulos: " + dto.getId());
		}
		
		User user = userDtoToUserParse.to(dto);
		
		User userSaved = service.save(user);	
		
		UserDto response = userToUserDtoParse.to(userSaved);
		
		// retorna http 201 para registro criado
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
}