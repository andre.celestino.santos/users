package br.com.vivo.users.parses;

import org.springframework.stereotype.Component;

import br.com.vivo.users.dtos.UserDto;
import br.com.vivo.users.dtos.UserTypeDto;
import br.com.vivo.users.models.User;
import br.com.vivo.users.models.UserType;

@Component
public class UserDtoToUserParse {

	public User to(UserDto dto) {
		User user = new User();
		user.setCity(dto.getCity());
		user.setCountry(dto.getCountry());
		user.setName(dto.getName());
		user.setState(dto.getState());
		user.setType(type(dto.getUserType()));
		return user;
	}
	
	private UserType type(UserTypeDto dto) {
		UserType userType = new UserType();
		userType.setId(dto.getId());
		return userType;
	}
	
}