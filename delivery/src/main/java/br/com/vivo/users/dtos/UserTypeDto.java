package br.com.vivo.users.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserTypeDto {
	
	@JsonProperty("codigo")
	private int id;

	@JsonProperty("descricao")
	private String descripton;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripton() {
		return descripton;
	}
	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}

}
