package br.com.vivo.users.parses;

import org.springframework.stereotype.Component;

import br.com.vivo.users.dtos.UserDto;
import br.com.vivo.users.dtos.UserTypeDto;
import br.com.vivo.users.models.User;
import br.com.vivo.users.models.UserType;

@Component
public class UserToUserDtoParse {

	public UserDto to(User user) {
		UserDto dto = new UserDto();
		dto.setCity(user.getCity());
		dto.setCountry(user.getCountry());
		dto.setId(user.getId());
		dto.setName(user.getName());
		dto.setState(user.getState());
		dto.setUserType(to(user.getType()));
		return dto;
	}
	
	private UserTypeDto to(UserType type) {
		UserTypeDto userTypeDto = new UserTypeDto();
		userTypeDto.setDescripton(type.getDescripton());
		userTypeDto.setId(type.getId());
		return userTypeDto;
	}
	
}