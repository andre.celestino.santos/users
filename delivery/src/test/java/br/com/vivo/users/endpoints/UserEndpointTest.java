package br.com.vivo.users.endpoints;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import br.com.vivo.users.dtos.UserDto;
import br.com.vivo.users.dtos.UserTypeDto;
import br.com.vivo.users.parses.UserDtoToUserParse;
import br.com.vivo.users.parses.UserToUserDtoParse;
import br.com.vivo.users.services.UserService;

@ExtendWith(SpringExtension.class)
public class UserEndpointTest {
	
	@InjectMocks
	private UserEndpoint userEndpoint;
	
	@Mock
	private UserService service;
	
	@Mock
	private UserDtoToUserParse userDtoToUserParse;

	@Mock
	private UserToUserDtoParse userToUserDtoParse;
	
	@Test
	public void saveCreated() {
		UserDto userDto = new UserDto();
		userDto.setCity("sao paulo");
		userDto.setCountry("Brasil");
		userDto.setName("Andre");
		userDto.setState("SP");
		UserTypeDto type = new UserTypeDto();
		type.setId(1); 
		userDto.setUserType(type);
		
		Mockito.when(userToUserDtoParse.to(null)).thenReturn(new UserDto());
		
		ResponseEntity<?> save = userEndpoint.save(userDto);
		
		Assertions.assertEquals(save.getStatusCode().value(), HttpStatus.CREATED.value());
		Assertions.assertNotNull(save.getBody());
	}
	
	@Test
	public void saveBadRequest() {
		UserDto userDto = new UserDto();
		userDto.setId(1);

		Assertions.assertThrows(ResponseStatusException.class, () -> userEndpoint.save(userDto));
	}
	

}